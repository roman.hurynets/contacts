export const createIsRequired = errorText => value => Boolean(value) || errorText

export const validateNameLength = value => value.length <= 10 || 'Name is not required'

export const validatePhoneNumber = value => {
  return /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/.test(value) || 'Number is not required'
}

export const validateEmail = value =>  {
  return /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(value) || 'E-mail is not required'
}
