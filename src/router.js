import Vue from 'vue'
import Router from 'vue-router'
import Contacts from './views/Contacts'
import ContactInfo from './components/ContactInfo/ContactInfo'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Contacts
    },
    {
      path: '/contact/:id',
      component: ContactInfo
    },
  ]
})
