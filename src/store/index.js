import Vue from 'vue'
import Vuex from 'vuex'
import contacts from './modules/contacts'
import { scaffoldStore } from 'undo-redo-vuex'

Vue.use(Vuex)

export default new Vuex.Store(scaffoldStore(contacts))
