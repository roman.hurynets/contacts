export const LS = {
  get: key => {
    const value = window.localStorage.getItem(key)
    try {
      return JSON.parse(value)
    } catch (e) {
      return isFinite(value) ? Number(value) : value
    }

  },
  set: (key, data) => window.localStorage.setItem(key, JSON.stringify(data)),
}
