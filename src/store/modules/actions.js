import {LS} from '../ls'
import { v4 as uuidv4 } from 'uuid';
import {
  UPDATE_CONTACT_LIST,
  SET_CURRENT_CONTACT,
  SET_SEARCH_TEXT,
  SET_EDITABLE
} from './constans'


export default {
  targetContact ({commit, state}, id) {
    const item = state.contacts.find(el => el.id === id)
    commit(SET_CURRENT_CONTACT, item)
  },

  removeContact ({commit, state}, id) {
    const item = state.contacts.filter(el => el.id !== id)
    commit(UPDATE_CONTACT_LIST, item)
  },

  addNewContact({commit, state}, item) {
    const contact = {
      ...item,
      id: uuidv4(),
      }
    commit(UPDATE_CONTACT_LIST, [...state.contacts].concat(contact))
  },

  searchContacts({commit, state}, query) {
    const items = LS.get('test').filter(el => el.name.includes(query))
    commit(SET_SEARCH_TEXT, query)
    commit(UPDATE_CONTACT_LIST, items)
  },

  toggleEditing({ commit, state }) {
    commit(SET_EDITABLE, !state.isEditing)
  },

  updateCurrentContact({commit, state}, {key, value, idx}) {
    if(idx === undefined) {
      const currentContact = {
        ...state.currentContact,
        [key]: value
      }
      commit(SET_CURRENT_CONTACT, currentContact)
    }

    const currentContact = {
      ...state.currentContact,
      [key]: state.currentContact[key].length - 1 < idx
        ? [...state.currentContact[key], value]
        :state.currentContact[key].map((el, i) => idx === i ? value : el)
    }
    commit(SET_CURRENT_CONTACT, currentContact)
  },

  saveChange({commit, state}, id){
    const contacts = state.contacts.map(el => el.id === id
      ? state.currentContact
      : el )
    commit(UPDATE_CONTACT_LIST, contacts)
    LS.set('test', [...state.contacts])
  },

  removeContactDate({commit, state}, {key, idx}){
    const currentContact = {
      ...state.currentContact,
      [key]: state.currentContact[key].filter((el, index) => index !== idx)
    }
    commit(SET_CURRENT_CONTACT, currentContact)
  },

  setLocalInfo({state}) {
    LS.set('test', [...state.contacts])
  }
}
