export const UPDATE_CONTACT_LIST = 'updateContactList'
export const SET_CURRENT_CONTACT = 'setCurrentContact'
export const SET_SEARCH_TEXT = 'setSearchText'
export const SET_EDITABLE = 'setEditable'
