import mutations from './mutations'
import getters from './getters'
import actions from './actions'
import state from './state'
import undoRedo from "undo-redo-vuex";
import {
  UPDATE_CONTACT_LIST,
  SET_SEARCH_TEXT,
  SET_EDITABLE
} from './constans'

export default {
  mutations,
  getters,
  actions,
  state,
  plugins: [ undoRedo({
    ignoreMutations: [
      UPDATE_CONTACT_LIST,
      SET_SEARCH_TEXT,
      SET_EDITABLE
    ]
  })  ]
}
