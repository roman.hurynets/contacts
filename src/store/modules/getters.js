export default {
  allContacts(state) {
    return state.contacts
  },

  isEditableContact(state) {
    return state.isEditing
  },
}
