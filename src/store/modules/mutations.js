export default {
  updateContactList(state, contacts) {
    state.contacts = contacts
  },

  setCurrentContact(state, contact) {
    state.currentContact = contact
  },

  setSearchText(state, text) {
    state.query = text
  },
  setEditable(state, val) {
    state.isEditing = val
  },
}
