import {LS} from '../ls'

export default {
  contacts: LS.get('test') || [],
  isEditing: false,
  query: '',
  currentContact: {},
}
